object Constants {
  val instanceDeployedCSV = "scheduling_preliminary_instance_deploy_20180606.csv"
  val appResourcesCSV = "scheduling_preliminary_app_resources_20180606.csv"
  val appInterferenceCSV = "scheduling_preliminary_app_interference_20180606.csv"
  val agentResourceCSV = "scheduling_preliminary_machine_resources_20180606.csv"

  val appInterferenceB = "scheduling_preliminary_b_app_interference_20180726.csv"
  val appResourcesB = "scheduling_preliminary_b_app_resources_20180726.csv"
  val instanceDeployedB = "scheduling_preliminary_b_instance_deploy_20180726.csv"
  val agentResourceB = "scheduling_preliminary_b_machine_resources_20180726.csv"
}
