
import Models.{AgentResource, Instance, TaskResource}
import org.apache.spark.sql.SparkSession

import scala.collection.mutable

object SparkScheduler {

  def main(args: Array[String]): Unit = {

    def compute(appResource: String, agentResourceCsv: String, instanceDeployedCsv: String) = {
      val spark = SparkSession
        .builder()
        .appName("Spark SQL basic example")
        .master("local")
        .config("spark.some.config.option", "some-value")
        .getOrCreate()

      import spark.implicits._

      val appResourceMap =
        spark.read.format("csv")
          .option("inferSchema", "true")
          .load(appResource)
          .select($"_c0".as("name"),
            $"_c1".as("cpu"),
            $"_c2".as("memory"),
            $"_c3".as("disk"),
            $"_c4".as("p"),
            $"_c5".as("m"),
            $"_c6".as("pm")
          ).as[TaskResource].map(t => t.name -> t).collect().toMap

      val machineResources =
        spark.read.format("csv")
          .option("inferSchema", "true")
          .load(agentResourceCsv)
          .select($"_c0".as("name"),
            $"_c1".as("cpu"),
            $"_c2".as("memory"),
            $"_c3".as("disk"),
            $"_c4".as("p"),
            $"_c5".as("m"),
            $"_c6".as("pm")
          )
          .as[AgentResource].map(t => t.name -> t).collect()

      val instances =
        spark.read.format("csv")
          .option("inferSchema", "true")
          .load(instanceDeployedCsv)
          .select($"_c0".as("name"),
            $"_c1".as("appName"))
          .as[Instance].collect()

      spark.stop()

      val agentTaskSet = machineResources.map {
        _._1 -> mutable.Set.empty[String]
      }.toMap

      val machineResourceMap = machineResources.toMap

      val instanceCandidateList = instances.par.map { t =>
        t.name -> (mutable.Set() ++ agentTaskSet.keys)
      }.toMap

      val instanceMap = instances.map(t => t.name -> t.appName).toMap

      def getTaskResource(instance: String) = {
        appResourceMap(instanceMap(instance))
      }

      def getAgentResource(agent: String) = {
        machineResourceMap(agent)
      }

      def selectAgentFromCandidates(instance: String, agents: collection.Set[String]): String = {
        val p = Math.random()
        var accumulator = 0.0

        val total = agents.iterator.map { agent =>
          getAgentResource(agent).cpu * 1.0
        }.sum

        val maybeResult = agents.find { agent =>
          accumulator += getAgentResource(agent).cpu * 1.0 / total
          p <= accumulator
        }
        maybeResult.getOrElse(maybeResult.head)
      }

      def agentResourceExceeded(agent: String, tasks: mutable.Set[String]): Boolean = {
        val agentResource = getAgentResource(agent)
        val disk = tasks.map(getTaskResource(_).disk).sum >= agentResource.disk
        val cpu = tasks.map(getTaskResource(_).cpuArray).fold(new Array[Double](98))((a, b) => (a zip b).map(t => t._1 + t._2))
          .exists(c => c >= agentResource.cpu)
        val memory = tasks.map(getTaskResource(_).memoryArray).fold(new Array[Double](98))((a, b) => (a zip b).map(t => t._1 + t._2))
          .exists(c => c >= agentResource.memory)
        cpu || memory || disk
      }

      instances.foreach { instance =>
        val agent: String = selectAgentFromCandidates(instance.name, instanceCandidateList(instance.name))
        agentTaskSet(agent).add(instance.name)
        //todo 如果机器上面的硬件超过资源限制，则将agent从任何包含它的列表中移除。
        if (agentResourceExceeded(agent, agentTaskSet(agent))) {
          instanceCandidateList.keys.par.foreach { key =>
            instanceCandidateList(key).remove(agent)
          }
        }
      }

      def computeCost(agentTaskSet: Map[String, mutable.Set[String]]) = {
        agentTaskSet.map { case (agent, tasks) =>
          val agentResource = getAgentResource(agent)
          val agentScore = if (tasks.isEmpty) 0.0
          else {
            tasks.map { task =>
              val taskResource = getTaskResource(task)
              taskResource.cpuArray
            }.fold(new Array[Double](98)) { (a, b) =>
              a.zip(b).map(t => t._1 + t._2)
            }.map { cpuUsage =>
              val cpu = cpuUsage / agentResource.cpu - 0.5
              val x = if (cpu < 0) 0 else cpu
              val cost = 1 + 10 * (math.exp(x) - 1)
              cost
            }.sum / 98
          }
          agentScore
        }.sum
      }

      println(
        computeCost(agentTaskSet)
      )
    }

//    compute(Constants.appResourcesCSV, Constants.agentResourceCSV, Constants.instanceDeployedCSV)
    compute(Constants.appResourcesB, Constants.agentResourceB, Constants.instanceDeployedB)
  }
}
