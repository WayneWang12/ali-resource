
object Models {

  case class AgentResource(name: String, cpu: Double, memory: Double, disk: Double, p: Int, m: Int, pm: Int)

  case class TaskResource(name: String, cpu: String, memory: String, disk: Double, p: Int, m: Int, pm: Int) {
    val cpuArray: Array[Double] = cpu.split('|').map(_.toDouble)
    val memoryArray: Array[Double] = cpu.split('|').map(_.toDouble)
  }

  case class Instance(name: String, appName: String)

}
