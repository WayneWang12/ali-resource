name := "scheduler"

version := "0.1"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "com.google.guava" % "guava" % "25.1-jre",
  "org.apache.commons" % "commons-lang3" % "3.7",
  "org.apache.spark" %% "spark-sql" % "2.3.1",
  "org.apache.spark" %% "spark-mllib" % "2.3.1"
)

